/**
 */

#ifndef UTILS_MY_THREAD_H_
#define UTILS_MY_THREAD_H_

#include <atomic>
#include <cstdint>
#include <functional>
#include <mutex>
#include <string>
#include <thread>
#include <vector>
#include <condition_variable>
#include <memory>


namespace MyTest {
class MyThread {
 public:
  explicit MyThread(const std::string& in_name, int32_t in_priority, uint32_t in_worker_num,
                     const std::function<void()> in_func, int32_t in_cpusetsize, const cpu_set_t*const in_cpuset);
  ~MyThread();
  void thread_start();
  void thread_shutdown();
  bool has_shutdown() const {
    return is_shutdown_;
  }
  void timed_wait(uint32_t useconds);
  static void set_self_thread_priority(int32_t in_priority);

 private:
  static void my_thread_func_(MyThread* thread_ins);

 private:
  std::string thread_name_;
  int32_t thread_priority_;
  uint32_t thread_worker_num_;
  std::vector<std::shared_ptr<std::thread>> my_threads_;
  std::function<void()> func_main_;
  int32_t thread_cpusetsize_;
  const cpu_set_t* thread_cpuset_ = nullptr;
  std::mutex thread_mutex_;
  std::condition_variable thread_cond_;
  bool is_shutdown_;
};

}  // namespace MyTest

#endif  // UTILS_MY_THREAD_H_
